#include <iostream>
#include <fstream>
#include <filesystem>
#include <getopt.h>
#include "sd9.h"

namespace fs = std::filesystem;

bool customVolume = false;
uint16_t volume = 0;

bool customLoopStart = false;
uint32_t loopStart = 0;

bool customLoopEnd = false;
uint32_t loopEnd = 0;
std::string directoryName = "";

static void showUsage(std::string name) {
    std::cout << "Usage: " << name << " [option(s)] desiredSound.wav targetSd9.sd9\n"
              << "Options:\n"
              << "\t-h, --help\t\t\tShow this help message\n"
              << "\t-V, --volume [0-126]\t\tUse custom volume value in header, lower is louder, 126 is mute\n"
              << "\t-S, --loop-start [VALUE]\tUse custom loop start value in header\n"
              << "\t-E, --loop-end [VALUE]\t\tUse custom loop end value in header\n"
              << "\t-d, --directory [DIRECTORY]\tOutput result to a directory"
              << std::endl;
} // TODO: value explanation in README

static int processArguments(int argc, char* argv[]);

int main(int argc, char* argv[]) {

    int argIndex = processArguments(argc, argv);
    if (argIndex < 0) {
        return 0;
    }

    if (argIndex + 1 >= argc) {
        std::cerr << "Sound and target sd9 files are required!" << std::endl;
        return 1;
    }

    // get sound file and size
    std::string sourceSoundFilename = argv[argIndex];
    fs::path sourceSoundPath(sourceSoundFilename);
    if (!fs::exists(sourceSoundPath)) {
        std::cerr << "File " << sourceSoundFilename << " does not exist!" << std::endl;
        return 1;
    }
    uint32_t soundSize = fs::file_size(sourceSoundPath);

    std::ifstream sourceSound(sourceSoundFilename, std::ios::binary);
    if (!sourceSound.good()) {
        std::cerr << "Can't open file " << sourceSoundFilename << std::endl;
        return 1;
    }
    ++argIndex;

    // get sd9 file and verify header
    std::string baseSd9Arg = argv[argIndex];
    fs::path baseSd9Path(baseSd9Arg);
    std::ifstream baseSd9(baseSd9Arg, std::ios::binary);
    if (!baseSd9.good()) {
        std::cerr << "Can't open file " << baseSd9Arg << std::endl;
        return 1;
    }
    sd9Header_t header = readHeader(baseSd9);
    if (!verifyHeader(header)) {
        std::cerr << baseSd9Arg << " is not a valid SD9 file!" << std::endl;
        return 1;
    }
    ++argIndex;

    // override values in header
    header.wavSize = soundSize;
    if (customVolume) {
        header.volume = volume;
    }
    if (customLoopStart) {
        header.loopStart = loopStart;
    }
    if (customLoopEnd) {
        header.loopEnd = loopEnd;
    }

    // combine header+sound to output sd9 file
    std::string outputFilename = directoryName + "new_" + baseSd9Path.filename().string();
    std::ofstream output(outputFilename, std::ios::binary);
    output.write((char*)&header, sizeof(header));
    output << sourceSound.rdbuf();
    output.close();
    output.clear();

    return 0;
}

static int processArguments(int argc, char* argv[]) {
    if (argc <= 1) {
        showUsage(argv[0]);
        return -1;
    }

    int argIndex = 1;

    const char* const short_opts = "hV:S:E:d:";
    const option long_opts[] = {
        {"help", no_argument, nullptr, 'h'},
        {"volume", required_argument, nullptr, 'V'},
        {"loop-start", required_argument, nullptr, 'S'},
        {"loop-end", required_argument, nullptr, 'E'},
        {"directory", required_argument, nullptr, 'd'},
        {nullptr, no_argument, nullptr, 0}
    };
    int opt = 0;
    int longIndex = 0;

    while ((opt = getopt_long(argc, argv, short_opts, long_opts, &longIndex)) != -1) {

        switch (opt) {
            case 'h': // help
                showUsage(argv[0]);
                return -1;

            case 'V': // volume
                argIndex += 2;
                volume = std::stoul(optarg, nullptr, 10);
                if (volume < 0 || volume > 126) {
                    std::cerr << "Volume has to be in range 0-126!" << std::endl;
                    return -1;
                }
                customVolume = true;
                break;

            case 'S': // loop-start
                argIndex += 2;
                loopStart = std::stoul(optarg, nullptr, 10);
                customLoopStart = true;
                break;

            case 'E': // loop-end
                argIndex += 2;
                loopEnd = std::stoul(optarg, nullptr, 10);
                customLoopEnd = true;
                break;

            case 'd': // directory
                argIndex += 2;
                directoryName = optarg;
                fs::path directory(directoryName);
                if (!fs::exists(directory)) {
                    fs::create_directory(directory);
                }
                directoryName += "/";
                break;
        }
    }

    return argIndex;
}
