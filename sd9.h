#ifndef _SD9_H
#define _SD9_H

typedef struct {
    uint32_t sd; // "SD9." 0x00394453 [53 44 39 00]
    uint32_t headerSize; // always dec 32, 0x00000020 [20 00 00 00]
    uint32_t wavSize; // MS ADCPM sound size
    uint32_t unknown1; // always 0x00013231 or 0xFFFF3231
    uint16_t unknown2; // always 0x0040
    uint16_t volume; // from 0x0000 to 0x007D, lower louder
    uint32_t loopStart;
    uint32_t loopEnd;
    uint16_t loopEnable; // 0x0000 disable, 0x0001 enable
    uint16_t soundIndex;
} sd9Header_t;

bool verifyHeader(const sd9Header_t header);

sd9Header_t readHeader(std::ifstream& file);

#endif // _SD9_H
